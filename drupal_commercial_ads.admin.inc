<?php


/**
 * @file
 * Admin configuration menu.
 */

/**
 * create the add form for add's
 * @,see custom_add_add_create_form_validate()
 * @,see custom_add_add_create_form_submit()
 */
function drupal_commercial_ads_add_create_form($form, &$form_state) {
    $form = array();
    $form['page_name'] = array(
        '#title' => t('Enter the Page Name'),
        '#description' => t('Enter the Page Name here. For eg. For home page : home, For Search Page : search'),
        '#type' => 'textfield',
        '#required' => TRUE,
    );
    $form['page_link'] = array(
        '#title' => t('Enter the Page URL'),
        '#description' => "For Home page : 'home'",
        '#type' => 'textfield',
        '#required' => TRUE,
    );
    $form['height'] = array(
        '#title' => t('Height'),
        '#description' => t('Height of Add Block in px'),
        '#type' => 'textfield',
        '#size' => 4,
        '#required' => TRUE,
        '#maxlength' => 5,
    );
    $form['width'] = array(
        '#title' => t('Width'),
        '#description' => t('Height of Add Block in px'),
        '#type' => 'textfield',
        '#size' => 4,
        '#required' => TRUE,
    );
    $form['amount'] = array(
        '#title' => t('Price'),
        '#description' => t('chrages per day in British Pound'),
        '#type' => 'textfield',
        '#size' => 4,
        '#required' => TRUE,
    );
    $form['link'] = array(
        '#title' => t('Link'),
        '#description' => t('Default link of the page'),
        '#type' => 'hidden',
        '#default_value' => '/user/adds',
        '#required' => TRUE,
    );
    $form['image'] = array(
        '#type' => 'file',
        '#title' => t('Upload Picture'),
        '#description' => t('Default picture for add block'),
        '#size' => 48,
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );
    return($form);
}

/**
 * Validation handler
 * @,see drupal_commercial_ads_add_create_form()
 * @.see drupal_commercial_ads_add_create_form_submit()
 */
function drupal_commercial_ads_add_create_form_validate(&$form, &$form_state) {
    //for image field
    $height = $form_state['values']['height'];
    $width = $form_state['values']['width'];
    $validators = array(
        'file_validate_is_image' => array('png'),
    );
    if ($file = file_save_upload('image', $validators)) {
        $info = image_get_info($file->uri);
        
        if(($height != $info['height']) || ($width != $info['width'])) {
            $msg = 'Please enter the image of size '.$width.'X'.$height;
            form_set_error('image', $msg);
        }
    }
    else {
        form_set_error('image', t("Failed to upload the picture image."));
    }
}

/**
 * Submission handler
 * @,see custom_add_add_create_form()
 * @,see custom_add_add_create_form_validate()
 */
function drupal_commercial_ads_add_create_form_submit(&$form, &$form_state) {
    //create a block first of predefined size;
    $validators = array(
        'file_validate_is_image' => array('png'),
    );
    //get all values
    $height = $form_state['values']['height'];
    $width = $form_state['values']['width'];
    $page_name = $form_state['values']['page_name'];
    $page_link = $form_state['values']['page_link'];
    $amount = $form_state['values']['amount'] * 100;
    $title = $page_name."/".$page_link."(".$width."X".$height.")";
    //check for page_url
    if($page_link == 'home') {
        $page_link = "<front>";
    }
    //create instance of this block into custom_add table
    //dsm("INSERT INTO custom_add(page_name, page_link, height, width, title, amount,block_id) values('".$page_name."','".$page_link."',".$height.",".$width.",'".$title."','".$amount."','blank')");
    db_query("INSERT INTO custom_add(page_name, page_link, height, width, title, amount,block_id) values('".$page_name."','".$page_link."',".$height.",".$width.",'".$title."','".$amount."','blank')");
    
    //get the add_id of the current add block
    $add_id = db_select('custom_add','c')
        ->fields('c', array('add_id'))
        ->condition('title', $title, '=')
        ->orderBy('add_id', 'DESC')
        ->range(0, 1)
        ->execute()
        ->fetchAssoc();
    //dsm($add_id);
    
    //check for page_url
    if($page_link == 'home') {
        $page_link = "<front>";
    }
    //create block_id
    $block_id = "custom_add_".$add_id['add_id'];
    
    //upload image
    $file = file_save_upload('image', $validators);
    $file->status   = FILE_STATUS_PERMANENT;
    $file = file_save_upload('image', $validators);
    $destination = 'public://'.$file->filename;
    if (file_copy($file, $destination, FILE_EXISTS_RENAME)) {
        $info = image_get_info($file->uri);
        //create the default node for the Add block.
        $node = new stdClass();
        $node->uid = 1;
        $node->type = 'adds_content';
        $node->language = 'und';
        $node->is_new = 1;
        $node->title = "Custom_Add";
        //$node->field_link['und'][0]['url'] = $form_state['values']['link'];
        $node->field_image['und'][0] = array(
            'fid' => $file->fid,
            'alt' => '',
            'title' => '',
            'width' => $info['width'],
            'height' => $info['height'],
            'uid' => $file->uid,
            'filename' => $file->filename,
            'uri' => $destination,
            'filemime' => $file->filemime,
            'filesize' => $file->filesize,
            'status' => '1',
            'timestamp' => $file->timestamp,
        );
        //dsm($node);
        node_save($node);
        //dsm('ss');
        $default_nid = db_select('node', 'n')
            ->fields('n', array('nid'))
            ->condition('type', 'adds_content', '=')
            ->orderBy('created', 'DESC')
            ->execute()
            ->fetchAssoc();
        //update the default nid for this block
        //dsm("update custom_add set default_node_id=".$default_nid['nid'].", block_id='".$block_id."' where add_id=".$add_id['add_id']);
        db_query("update custom_add set default_node_id=".$default_nid['nid'].", block_id='".$block_id."' where add_id=".$add_id['add_id']);
        global $base_url;
        drupal_set_message('A new Ad is created');
        drupal_goto($base_url."/admin/config");
    }
}

/**
 * for custom_add_add_edit_form()
 * create a form for view all add blocks.
 */
function drupal_commercial_ads_add_view_form() {
    global $base_url;
    $result_custom = db_select('custom_add', 'c')
        ->fields('c')
        ->execute()
        ->fetchAll();
    $rows = array();
    $header = array(
        array('data' => 'Add Id'),
        array('data' => 'Page Name'),
        array('data' => 'Size'),
        array('data' => 'Price'),
        array('data' => 'Block Name'),
        array('data' => 'Actions'),
        );
    if($result_custom) {
        foreach ($result_custom as $key => $value) {
            $price = $value->amount/100;
            $rows[] = array(
                array('data' => $value->add_id),
                array('data' => $value->page_name),
                array('data' => $value->width.'X'.$value->height),
                array('data' => $price),
                array('data' => $value->block_id),
                array('data' => "<span><a href = '".$base_url."/admin/config/custom_add/".$value->add_id."/view'>view</a></span><span><a href = '".$base_url."/admin/config/custom_add/".$value->add_id."/edit'>edit</a></span>"),
                );
            //"<span><a href = '".$base_url."/admin/config/custom_add/".$value->add_id."/view'>view</a></span><span><a href = '".$base_url."/admin/config/custom_add/".$value->add_id."/edit'>edit</a></span>"
        }
        $result = theme('table', array('header' => $header, 'rows'=> $rows));
    }
    else {
        drupal_set_message(t('There is no Ad block created yet.'));
        $result = "There is no Ad block created yet.";
    }
    return $result;
}
/**
 * To view all add's added by all users.
 */
function drupal_commercial_ads_add_view_all_form() {
    global $base_url;
    $result_custom = db_select('custom_add_user', 'c')
        ->fields('c')
        ->orderBy('add_id', 'DESC')
        ->execute()
        ->fetchAll();
    $rows = array();
    $header = array(
        array('data' => 'Id'),
        array('data' => 'User Name'),
        array('data' => 'Start Date'),
        array('data' => 'End Date'),
        array('data' => 'Status'),
        array('data' => 'Actions'),
    );
    if($result_custom) {
        foreach ($result_custom as $key => $value) {
            $price = $value->amount/100;
            $user_info = user_load($value->user_id);
            $status;
            switch($value->status) {
                case 0:
                    $status = 'Pending';
                    break;
                case 1:
                    $status = 'In Process';
                    break;
                case 2:
                    $status = 'Complete';
                    break;
                case 3:
                    $status = 'Stopped';
                    break;
            }
            $rows[] = array(
                array('data' => $value->add_id),
                array('data' => $user_info->name),
                array('data' => date('Y-m-d', $value->start_date)),
                array('data' => date('Y-m-d', $value->end_date)),
                array('data' => $status),
                array('data' => "<span><a href = '".$base_url."/admin/config/custom_add/add/".$value->id."/view'>view</a></span>"),
                );
        }
        $result = theme('table', array('header' => $header, 'rows'=> $rows));
    }
    else {
        drupal_set_message(t('There is no Ad created yet.'));
        $result = "<p>There is no Ad created yet.</p>";
    }
    return $result;
}
/**
 * create  screen for particular add
 */
function drupal_commercial_ads_add_view_particular_add($id) {
    //dsm('dd');
    $id = arg(4);
    global $base_url;
    $output = custom_add_add_view_particular_form($id);
    $result_custom = db_select('custom_add_user', 'c')
        ->fields('c')
        ->condition('id', $id, '=')
        ->execute()
        ->fetchAssoc();
    //check for status of add
    if($result_custom['status'] == 1) {
        $a = "<a href = '".$base_url."/admin/config/custom_add/add/".$id."/delete'>Stop</a>";
        $output .= "<div id = 'stop_add'>".$a."</div>";
    }
    return $output;
}
/**
 * for get the details about particular add
 * added by user.
 */
function drupal_commercial_ads_add_view_particular_form($id) {
    global $base_url;
    //dsm($id);
    $id = arg(4);
    //dsm($id);
    $result_custom = db_select('custom_add_user', 'c')
        ->fields('c')
        ->condition('id', $id, '=')
        ->execute()
        ->fetchAssoc();
    $add_id = $result_custom['add_id'];
    $result = db_select('custom_add', 'c1')
        ->fields('c1')
        ->condition('add_id', $add_id, '=')
        ->execute()
        ->fetchAssoc();
    $rows = array();
    $status;
    switch($result_custom['status']) {
        case 0:
            $status = 'Pending';
            break;
        case 1:
            $status = 'In Process';
            break;
        case 2:
            $status = 'Complete';
            break;
        case 3:
            $status = 'Stopped';
            break;
    }
    $user_info = user_load($result_custom['user_id']);
    $header = array(
        array('data' => 'Label'),
        array('data' => 'Value'),
        );
    $node = node_load($result_custom['node_id']);
    $msg;
    if($node) {
        $msg = "<img src = '".$base_url."/sites/default/files/".$node->field_image['und'][0]['filename']."'/>";
    }
    else {
        $msg = "There is no image uploaded yet for this Ad";
    }
    //$price = $result['amount']/100;
    $rows[] = array(
        array('data' => 'Id'),
        array('data' => $result_custom['id']),
    );
    $rows[] = array(
        array('data' => 'User Name'),
        array('data' => $user_info->name),
    );
    $rows[] = array(
        array('data' => 'Start Date'),
        array('data' => date('Y-m-d', $result_custom['start_date'])),
    );
    $rows[] = array(
        array('data' => 'End Date'),
        array('data' => date('Y-m-d', $result_custom['end_date'])),
    );
    $rows[] = array(
        array('data' => 'Size'),
        array('data' => $result['width']."X".$result['height']),
    );
    $rows[] = array(
        array('data' => 'Status'),
        array('data' => $status),
    );
    $rows[] = array(
        array('data' => 'Image'),
        array('data' => $msg),
    );
    return theme('table', array('header' => $header, 'rows'=> $rows));
}

/**
 * create form for stop the add
 * @,see custom_add_add_view_particular_stop_form_submit()
 */
function drupal_commercial_ads_add_view_particular_stop_form($form, &$form_state) {
    $id = arg(4);
    //dsm($id);
    $form['id'] = array(
        '#type' => 'value',
        '#value' => $id,
    );
    //$form['submit'] = array(
    //    '#value' => 'Stop',
    //    '#type' => 'submit',
    //);
    return confirm_form($form, t('Are you sure you want to Stop Ad @id?', array('@id' => $id)), 'admin/config/custom_add', NULL, t('Stop'));
}

/**
 * submission form handler
 * @,see custom_add_add_view_particular_stop_form()
 */ 
function drupal_commercial_ads_add_view_particular_stop_form_submit($form, &$form_state) {
    $id = $form_state['values']['id'];
    //dsm($form_state);
    $add_id = db_select('custom_add_user', 'c')
        ->fields('c')
        ->condition('id', $id, '=')
        ->execute()
        ->fetchAssoc();
    //update custom_add
    //dsm($add_id);
    db_query("update custom_add set node_id=0, start_date=0, end_date=0 where add_id=".$add_id['add_id']);
    
    //update custom_add_user
    db_query("update custom_add_user set status=3  where id=".$id);
    global $base_url;
    drupal_goto($base_url."/admin/config/custom_add");
}

/**
 * For particular add view
 */
function drupal_commercial_ads_particular_add_view($add_id) {
    global $base_url;
    $result_custom = db_select('custom_add', 'c')
        ->fields('c')
        ->condition('add_id', $add_id, "=")
        ->execute()
        ->fetchAssoc();
    //dsm($result_custom);
    $rows = array();
    $header = array(
        array('data' => 'Label'),
        array('data' => 'Value'),
        );
    $node = node_load($result_custom['default_node_id']);
    $price = $result_custom['amount']/100;
    $rows[] = array(
        array('data' => 'Add Id'),
        array('data' => $result_custom['add_id']),
    );
    $rows[] = array(
        array('data' => 'Page Name'),
        array('data' => $result_custom['page_name']),
    );
    $rows[] = array(
        array('data' => 'Page URL'),
        array('data' => $result_custom['page_link']),
    );
    $rows[] = array(
        array('data' => 'Width'),
        array('data' => $result_custom['width']),
    );
    $rows[] = array(
        array('data' => 'Height'),
        array('data' => $result_custom['height']),
    );
    $rows[] = array(
        array('data' => 'Block Name'),
        array('data' => $result_custom['block_id']),
    );
    $rows[] = array(
        array('data' => 'Price'),
        array('data' => $price." $"),
    );
    $rows[] = array(
        array('data' => 'Default Image'),
        array('data' => "<img src = '".$base_url."/sites/default/files/".$node->field_image['und'][0]['filename']."'/>"),
    );
    return theme('table', array('header' => $header, 'rows'=> $rows));
}

/**
 * perform for particular add edit form
 */
function drupal_commercial_ads_edit_form($form, &$form_state, $add_id) {
    $result_custom = db_select('custom_add', 'c')
        ->fields('c')
        ->condition('add_id', $add_id, "=")
        ->execute()
        ->fetchAssoc();
    $node = node_load($result_custom['default_node_id']);
    $form['page_name'] = array(
        '#title' => t('Page Name'),
        '#description' => t('Now You can not alter this.'),
        '#type' => 'item',
        '#markup' => $result_custom['page_name'],
    );
    $form['page_link'] = array(
        '#title' => t('Page URL'),
        '#description' => t('Now You can not alter this.'),
        '#type' => 'item',
        '#markup' => $result_custom['page_link'],
    );
    $form['height'] = array(
        '#title' => t('Height'),
        '#description' => t('Now You can not alter this.'),
        '#type' => 'item',
        '#size' => 4,
        '#markup' => $result_custom['height'],
    );
    $form['width'] = array(
        '#title' => t('Width'),
        '#description' => t('Now You can not alter this.'),
        '#type' => 'item',
        '#size' => 4,
        '#markup' => $result_custom['width'],
    );
    $form['amount'] = array(
        '#title' => t('Price'),
        '#description' => t('chrages per day in British Pound'),
        '#type' => 'textfield',
        '#default_value' => $result_custom['amount']/100,
        '#size' => 4,
        '#required' => TRUE,
    );
    $form['node_id'] = array(
        '#title' => t('Link'),
        '#description' => t('Default link of the page'),
        '#type' => 'hidden',
        '#default_value' => $result_custom['default_node_id'],
        //'#required' => TRUE,
    );
    $form['pre_image'] = array(
        '#title' => t('Image'),
        '#type' => 'item',
        '#markup' => "<img src = '".$base_url."/sites/default/files/".$node->field_image['und'][0]['filename']."'/>"
    );
    $form['add_id'] = array(
        '#title' => t('Link'),
        '#description' => t('Default link of the page'),
        '#type' => 'hidden',
        '#default_value' => $result_custom['add_id'],
        //'#required' => TRUE,
    );
    $form['image_upload'] = array(
        '#type' => 'radios',
        '#title' => t('You want to change the image ??'),
        '#options' => array(
            '0' => 'Yes',
            '1' => 'No',
        ),
        '#default_value' => 1,
        '#ajax' => array(
            'callback' => 'custom_upload_image_ajax',
            'wrapper'  => 'upload_new_image',
            'progress' => array('type' => 'throbber'),
          ),
    );
    $form['image'] = array(
        '#prefix' => "<div id = 'upload_new_image'>",
        '#suffix' => "</div>",
        '#tree' => TRUE,
        '#weight' => 100,
    );
    //dsm($form_state);
    if(($form_state['values']['image_upload'] == 0) && (isset($form_state['values']['image_upload']))) {
        $form['image'] = array(
            '#type' => 'file',
            '#title' => t('Upload Picture'),
            '#description' => t('Default Picture for Ad block'),
            '#size' => 48,
        );
    }
    
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#weight' => 110,
    );
    return($form);
}

/**
 * Ajax callback: upload new image.
 */
function custom_upload_image_ajax($form, &$form_state) {
    return $form['image'];
}

/**
 * Validation handler
 * @,see custom_add_edit_form()
 * @.see custom_add_edit_form_submit()
 */
function drupal_commercial_ads_edit_form_validate(&$form, &$form_state, $add_id) {
    //for image field
    if($form_state['values']['image_upload'] == 0) {
        $add_id = $form_state['values']['add_id'];
        //dsm($form_state);
        $result_custom = db_select('custom_add', 'c')
            ->fields('c')
            ->condition('add_id', $add_id, "=")
            ->execute()
            ->fetchAssoc();
        $height = $result_custom['height'];
        $width = $result_custom['width'];
        
        $validators = array(
            'file_validate_is_image' => array('png'),
        );
        if ($file = file_save_upload('image', $validators)) {
            $info = image_get_info($file->uri);
            
            if(($height != $info['height']) || ($width != $info['width'])) {
                $msg = 'Please enter the image of size '.$width.'X'.$height;
                form_set_error('image', $msg);
            }
        }
        else {
            form_set_error('image', t("Failed to upload the picture image."));
        }
    }
}
/**
 * Submission handler
 * @,see custom_add_edit_form()
 * @,see custom_add_edit_form_validate()
 */
function drupal_commercial_ads_edit_form_submit(&$form, &$form_state, $add_id) {
    //create a block first of predefined size;
    $add_id = $form_state['values']['add_id'];
    if($form_state['values']['image_upload'] == 0) {
        $validators = array(
            'file_validate_is_image' => array('png'),
        );
        //get all values
        $file = file_save_upload('image', $validators);
        $file->status   = FILE_STATUS_PERMANENT;
        $file = file_save_upload('image', $validators);
        $destination = 'public://'.$file->filename;
        if (file_copy($file, $destination, FILE_EXISTS_RENAME)) {
            $info = image_get_info($file->uri);
            //create the default node for the Add block.
            $node = node_load($form_state['values']['node_id']);
            $node->field_image['und'][0] = array(
                'fid' => $file->fid,
                'alt' => '',
                'title' => '',
                'width' => $info['width'],
                'height' => $info['height'],
                'uid' => $file->uid,
                'filename' => $file->filename,
                'uri' => $destination,
                'filemime' => $file->filemime,
                'filesize' => $file->filesize,
                'status' => '1',
                'timestamp' => $file->timestamp,
            );
            node_save($node);
        }
    }
    $price = $form_state['values']['amount'] * 100;
    db_query("update custom_add set amount = ".$price." where add_id=".$add_id);
}