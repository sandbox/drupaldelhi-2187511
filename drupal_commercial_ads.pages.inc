<?php

/** @file
 * Implements adds functionality specific to user.
 */

/**
 * Shows the previous adds(purchased) and
 * also provide functionality for buy new adds.
 */
function drupal_commercial_ads_user_page() {
    global $user;
    global $base_url;
    $uid = $user->uid;
    if($uid>0) {
        $output = "<span id = 'heading'>Ad Blocks Details : </span>";
        $output .= drupal_commercial_ads_purchased_adds_users();
        $output .= "<span id = 'heading'>Your Booked Ad Blocks : </span>";
        $output .= drupal_commercial_ads_purchased_adds($uid);
        //$output .= "<span class ='add_new_add'><a href = '".$base_url."/user/adds/add'>Add New Ad</a></span>";
    }
    else {
        $output = "You have to login first";
    }
    return $output;
}

/**
 * For show already purchased adds
 */
function drupal_commercial_ads_purchased_adds($uid) {
    $result_custom = db_select('custom_add_user', 'c')
        ->fields('c')
        ->condition('user_id', $uid, '=')
        ->execute()
        ->fetchAll();
    $rows = array();
    $header = array(
        array('data' => 'Add Id'),
        array('data' => 'Start Date'),
        array('data' => 'End Date'),
        array('data' => 'Status'),
        array('data' => 'Actions'),
        );
    if($result_custom) {
        $status;
        foreach ($result_custom as $key => $value) {
            switch($value->status) {
                case 0:
                    $status = 'Pending';
                    break;
                case 1:
                    $status = 'In Process';
                    break;
                case 2:
                    $status = 'Complete';
                    break;
                case 3:
                    $status = 'Stopped';
                    break;
            }
            $price = $value->amount/100;
            $rows[] = array(
                array('data' => $value->id),
                array('data' => date('Y-m-d', $value->start_date)),
                array('data' => date('Y-m-d', $value->end_date)),
                array('data' => $status),
                array('data' => "<span><a href = '".$base_url."/user/adds/".$value->id."/view'>view</a></span>"),
                );
            //"<span><a href = '".$base_url."/admin/config/custom_add/".$value->add_id."/view'>view</a></span><span><a href = '".$base_url."/admin/config/custom_add/".$value->add_id."/edit'>edit</a></span>"
        }
        $result = theme('table', array('header' => $header, 'rows'=> $rows));
    }
    else {
        drupal_set_message(t('There is no Ad created yet.'));
        $result = "There is no Ad created yet.";
    }
    return $result;
}
/**
 * for particular view of add for user
 */
function drupal_commercial_ads_add_user_page($id) {
    global $base_url;
    global $user;
    $uid = $user->uid;
     $result_custom = db_select('custom_add_user', 'c')
        ->fields('c')
        ->condition('user_id', $uid, '=')
        ->condition('id', $id, '=')
        ->execute()
        ->fetchAssoc();
    if($result_custom) {
        $rows = array();
        $header = array(
            array('data' => 'Label'),
            array('data' => 'Value'),
            );
        $status;
        switch($result_custom['status']) {
                case 0:
                    $status = 'Pending';
                    break;
                case 1:
                    $status = 'In Process';
                    break;
                case 2:
                    $status = 'Complete';
                    break;
                case 3:
                    $status = 'Stopped';
                    break;
            }
        $node = node_load($result_custom['node_id']);
        //$price = $result_custom['amount']/100;
        $rows[] = array(
            array('data' => 'Id'),
            array('data' => $result_custom['id']),
        );
        $rows[] = array(
            array('data' => 'Start Date'),
            array('data' => date('Y-m-d', $result_custom['start_date'])),
        );
        $rows[] = array(
            array('data' => 'End Date'),
            array('data' => date('Y-m-d', $result_custom['end_date'])),
        );
        $rows[] = array(
            array('data' => 'Status'),
            array('data' => $status),
        );
        $rows[] = array(
            array('data' => 'Image'),
            array('data' => "<img src = '".$base_url."/sites/default/files/".$node->field_image['und'][0]['filename']."'/>"),
        );
        return theme('table', array('header' => $header, 'rows'=> $rows));
    }
    else {
        drupal_goto($base_url);
    }
}
/**
 * For show already purchased adds for all users.
 */
function drupal_commercial_ads_purchased_adds_users() {
    $result_custom = db_select('custom_add', 'c')
        ->fields('c')
        ->execute()
        ->fetchAll();
    $rows = array();
    $header = array(
        array('data' => 'Page Name'),
        array('data' => 'Size'),
        array('data' => 'Status'),
        array('data' => 'Price'),
        array('data' => 'Buy'),
    );
    if($result_custom) {
        foreach ($result_custom as $key => $value) {
            $price = $value->amount/100;
            $nid = $value->node_id;
            $status = "";
            if($nid == 0) {
                $status = "Available";
            }
            else {
                $status = "Not Available";
            }
            if($status == "Available") {
                $purchased = "<a href = '/user/adds/add/".$value->add_id."'>Purchase</a>";
            }
            else{
                $purchased = '';
            }
            
            $rows[] = array(
                array('data' => $value->page_name),
                array('data' => $value->width.'X'.$value->height),
                array('data' => $status),
                array('data' => $price),
                array('data' => $purchased),
                );
            //"<span><a href = '".$base_url."/admin/config/custom_add/".$value->add_id."/view'>view</a></span><span><a href = '".$base_url."/admin/config/custom_add/".$value->add_id."/edit'>edit</a></span>"
        }
    $result = theme('table', array('header' => $header, 'rows'=> $rows));
    
}
else {
        drupal_set_message(t('There is no Ad block created yet.'));
        $result = "There is no Ad block created yet.";
    }
    return $result;
}

/** For purchase new adds
 * @, see custom_add_user_page()
 */
function drupal_commercial_ads_new_add($form, &$form_state, $add_id) {
    if($add_id) {
        $form['add'] = array(
            '#prefix' => '<div id="add_new_add">',
            '#suffix' => '</div>',
            '#tree'   => TRUE,
        );
        $form['add'] += array(
          '#type' => 'fieldset',
          '#title' => t('Purchase New Adds'),
          //'#description' => t('Select your Location.'),
          '#collapsible' => TRUE,
        );
          
        $value = db_select('custom_add', 'custom')
            ->fields('custom')
            ->condition('add_id', $add_id, '=')
            ->execute()
            ->fetchAssoc();
        $form['add']['select_area'] = array(
            //'#type' = 'markup',
            '#markup' => "<span>Selected Area : ".$value['title']."(".$value['width']."X".$value['height'].")"."</span>",
            '#prefix' => "<div id = 'selected_Area'>",
            '#suffix' => "</div>",
        );
        $form['add']['hidden'] = array(
            '#type' => 'hidden',
            '#default_value' => $add_id,
        );
        $form['add']['start_date'] = array(
            '#title' => t('Enter Start Date'),
            '#type' => 'date_popup',
            '#date_format' => 'Y-m-d',
            '#date_year_range' => '0:+1',
            '#weight' => 4,
            '#required' => TRUE,
        );
        $form['add']['end_date'] = array(
            '#title' => t('Enter End Date'),
            '#type' => 'date_popup',
            '#date_format' => 'Y-m-d',
            '#date_year_range' => '0:+1',
            '#weight' => 5,
            '#required' => TRUE,
        );
        $form['add']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
            '#weight' => 6,
        );
        //dsm($form);
        return $form;
    }
}

/**
 * Ajax callback: updates the customer search results.
 */
function drupal_commercial_ads_new_add_ajax($form, &$form_state) {
  //return $form['add']['adds_area'];
}
/**
 * form validation handler
 * @,see custom_add_new_add()
 */
function drupal_commercial_ads_new_add_validate(&$form, &$form_state) {
    $add_id = $form_state['values']['add']['hidden'];
    $end_date = strtotime($form_state['values']['add']['end_date']);
    $start_date = strtotime($form_state['values']['add']['start_date']);
    //get all info about add_id (custom_add).
    if(($start_date < REQUEST_TIME) && (date('d', $start_date)!=date('d',REQUEST_TIME))) {
        form_set_error('add][start_date', t('Start Date must be greator than Current Date.'));
    }
    if($start_date >= $end_date) {
        form_set_error('add][end_date', t('End Date must be greator than Start Date.'));
    }
    //dsm($form_state['values']['add']['end_date']);
    //dsm($form_state['values']['add']['start_date']);
    //dsm($end_date);
    //dsm($start_date);
    //dsm(date('Y-m-d h:i:s A',$end_date));
    //dsm(date('Y-m-d h:i:s A',$start_date));
    if($start_date && $end_date) {
    //dsm("select * from custom_add_user where end_date < ".$start_date." and payment=1 and node_id != 0 and add_id=".$add_id);
    $sql = db_query("select * from custom_add_user where status in (0,1) and payment = 1 and node_id != 0 and add_id=".$add_id)->fetchAll();
    //dsm($sql);
    foreach($sql as $key => $value) {
        if(($start_date < $value->start_date && $end_date > $value->end_date) ||
            ($start_date > $value->start_date && $start_date < $value->end_date) ||
            ($end_date > $value->start_date && $end_date < $value->end_date) ||
            ($start_date>$value->start_date && $end_date < $value->end_date)) {
                drupal_set_message(t('This add is already booked  part 4.'));
                form_set_error(t('This add is already booked.'));
            }
    }
    //form_set_error(t('This add is already booked.'));
}
}
/**
 * form submit handler
 * @,see custom_add_new_add()
 */
function drupal_commercial_ads_new_add_submit(&$form, &$form_state) {
    global $user;
    
    //dsm($form_state);
    $uid = $user->uid;
    //dsm($form_state);
    //dsm(commerce_product_load(4));
    $product = commerce_product_new('add_products');
    //dsm($product);
    $sql = db_select('commerce_product','c')
        ->fields('c',array('sku'))
        ->condition('type', 'add_products', '=')
        ->orderBy('product_id', 'DESC')
        ->range(0,2)
        ->execute()
        ->fetchAssoc();
    if($sql['sku']) {
        $sku = $sql['sku'] + 1;
    }
    else {
        $sku = 90009;
    }
    $add_id = $form_state['values']['add']['hidden'];
    //get all info about add_id (custom_add)
    $name = db_select('custom_add', 'custom')
        ->fields('custom')
        ->condition('add_id', $add_id, '=')
        ->execute()
        ->fetchAssoc();
    $end_date = strtotime($form_state['values']['add']['end_date']);
    $start_date = strtotime($form_state['values']['add']['start_date']);
    //calculate no. of days
    $datediff = $end_date - $start_date;
    $day = floor($datediff/(60*60*24));
    
    //empty the cart
    $cart_items = commerce_cart_order_load($user->uid);
    //dsm($cart_items);
    if(count($cart_items->commerce_line_items['und'])>0) {
        $order_id = commerce_cart_order_id($uid);
        //dsm($order_id);
        commerce_order_delete($order_id);
    }
    
    //create product for Adds
    $product->sku = $sku;
    $product->title = "Add Products".$sku;
    $product->uid = 1;
    $product->field_add_dates['und'][0]['value'] = $form_state['values']['add']['start_date'];
    $product->field_add_dates['und'][0]['value2'] = $form_state['values']['add']['end_date'];
    $product->commerce_price['und'][0]['amount'] = $day*$name['amount'];
    $product->commerce_price['und'][0]['currency_code'] = 'GBP';
    $p = commerce_product_save($product);
    if ($p) {
        //get product_id
        $product_id = db_select('commerce_product','c')
            ->fields('c',array('product_id'))
            ->orderBy('product_id', 'DESC')
            ->condition('type', 'add_products', '=')
            ->execute()
            ->fetchAssoc();
        //dsm($product_id);
        $product = commerce_product_load($product_id['product_id']);
        //dsm($product);
        //Add content into custom_Add_user table
        $sql = db_query("INSERT INTO custom_add_user(user_id, add_id, payment,start_date, end_date) values(".$user->uid.", ".$add_id.",0,".$start_date.",".$end_date.")");
        //drupal_set_message($sql);
        drupal_set_message("hii");
        //add product into cart
        $quantity = 1;
        $line_item = commerce_product_line_item_new($product, $quantity);
        $line_item = commerce_cart_product_add($uid, $line_item);
    }
}
/** For add information into adds
 * create content for content type (adds_content)
 * @, see custom_add_new_information_submit()
 */
function drupal_commercial_ads_new_information($form, &$form_state, $id) {
    $form['id'] = array(
        '#type' => 'hidden',
        '#default_value' => $id,
    );
    $form['title'] = array(
        '#type' => 'textfield',
        '#description' => t('Enter the title for the add'),
        '#required' => TRUE,
        '#title' => t('Enter Title'),
    );
    $form['link'] = array(
        '#type' => 'textfield',
        '#description' => t('Enter the link for the add'),
        '#required' => TRUE,
        '#title' => t('Enter Link'),
    );
    $form['image'] = array(
        '#type' => 'file',
        '#title' => t('Upload Picture'),
        '#size' => 48,
        //'#required' => TRUE,
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );
    return($form);
}

/**
 * Form validation Handler
 * @,see custom_add_new_information()
 */
function drupal_commercial_ads_new_information_validate($form, &$form_state) {
    $validators = array(
        'file_validate_is_image' => array('png'),
    );
    if ($file = file_save_upload('image', $validators)) {
        $info = image_get_info($file->uri);
        
        $id = $form_state['values']['id'];
        $sql1 = db_query("select * from custom_add_user where id = ".$id)->fetchAssoc();
        $sql = db_query('select * from custom_add where add_id ='.$sql1['add_id'])->fetchAssoc();
        if(($sql['height'] != $info['height']) || ($sql['width'] != $info['width'])) {
            $msg = 'Please enter the image of size '.$sql['width'].'X'.$sql['height'];
            dsm($msg);
            form_set_error('image', $msg);
        }
    }
    else {
        form_set_error('image', t("Failed to upload the picture image."));
    }
    
}
/**
 * Form submit handler
 * @,see custom_add_new_information()
 */
function drupal_commercial_ads_new_information_submit($form, &$form_state) {
    global $user;
    $validators = array(
        'file_validate_is_image' => array('png'),
    );
    $file = file_save_upload('image', $validators);
    $file->status   = FILE_STATUS_PERMANENT;
    $file = file_save_upload('image', $validators);
    //$file->filename = $file->filename.REQUEST_TIME;
    $destination = 'public://'.$file->filename;
    if (file_copy($file, $destination, FILE_EXISTS_RENAME)) {
        $node = new stdClass();
        $node->uid = $user->uid;
        $node->type = 'adds_content';
        $node->language = 'und';
        $node->is_new = 1;
        $node->title = $form_state['values']['title'];
        $node->field_link['und'][0]['url'] = $form_state['values']['link'];
        $node->field_image['und'][0] = array(
            'fid' => $file->fid,
            'alt' => '',
            'title' => '',
            'width' => $info['width'],
            'height' => $info['height'],
            'uid' => $file->uid,
            'filename' => $file->filename,
            'uri' => $destination,
            'filemime' => $file->filemime,
            'filesize' => $file->filesize,
            'status' => '1',
            'timestamp' => $file->timestamp,
        );
        node_save($node);
        $nid = db_select('node', 'n')
            ->fields('n', array('nid'))
            ->orderBy('created', 'DESC')
            ->range(0, 1)
            ->execute()
            ->fetchAssoc();
        $id = $form_state['values']['id'];
        $sql1 = db_query("select * from custom_add_user where id = ".$id)->fetchAssoc();
        $end_date = $sql1['end_date'];
        $start_date = $sql1['start_date'];
        
        //update the custom_add_user
        $update_sql = "update custom_add_user set node_id=".$nid['nid'].", status = 0 where id =".$id;
        
        db_query($update_sql);
    }
    else {
        form_set_error('image', t("Failed to upload the picture image1."));
    }
    global $base_url;
    drupal_goto($base_url);
}